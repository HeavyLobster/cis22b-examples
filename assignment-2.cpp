#include <iostream>
#include <fstream>
#include <string>

using std::string;

const string DICTFILE = "unsorted_dictionary.txt";
const string KEYSFILE = "keywords.txt";
const int NUMWORDS = 16000;
const int NUMKEYS = 84;

int main() {
    string dictWords[NUMWORDS];
    string keywords[NUMKEYS];
    bool keysFound[NUMKEYS];
    int numWordsNotFound = 0;

    // Put words into both arrays
    dictWords = get_words_from_file(DICTFILE);
    keywords = get_words_from_file(KEYSFILE);

    // Sort both arrays
    dictWords = sort_words(dictWords);
    keywords = sort_words(keywords);

    // Search for each dictionary words in the keywords
    for (int x = 0; x < NUMKEYS; x++) {
        // If a dictionary word is found in keywords, mark it found
        keysFound[x] = search_for(keywords[x], dictWords);
    }

    // Tally keywords not found and print them
    for (int x = 0; x < NUMKEYS; x++) {
        if (keysFound[x]) {
            std::cout << '"' << keywords[x] << "\" was not found"
                << std::endl;
            numWordsNotFound++;
        }
    }

    std::cout << numWordsNotFound << " keywords were not found"
        << std::endl;

    return 0;
}
